import requests
from bs4 import BeautifulSoup
import requests_html
import lxml.html as lh

from time import sleep


class Py3status:
    button_minimize = None
    button_next = None
    minimized = True
    format_minimized = ""
    format = "  {stock} : {price} {percent} "
    returned_format = format
    cache_timeout = 30
    stock_info = [ "IWDA.AS", "WKHS", "ARKK"]
    stock_info_index = 0
    stock = stock_info[stock_info_index]
    stock_display_name = stock_info[stock_info_index].split(".", 1)[0]

    def _get_text(self):
        self.stock = self.stock_info[self.stock_info_index]
        self.stock_display_name = self.stock_info[self.stock_info_index].split(".", 1)[0]
        try:
            url = 'https://in.finance.yahoo.com/quote/' + self.stock
            session = requests_html.HTMLSession()
            r = session.get(url)
            content = BeautifulSoup(r.content, 'lxml')
            price = str(content).split('data-reactid="32"')[4].split('</span>')[0].replace('>','')
            percent = str(content).split('data-reactid="33"')[3].split('</span>')[0].replace('>','').split(' ')[1]
            sign = percent[1]
        except:
            price = 0.00
            percent = "(-0.00)"
            sign = "-"
        try:
            price = float(price.replace(',',''))
            color = self.py3.COLOR_GOOD
        except ValueError as e:
            price = 0.00
            color = self.py3.COLOR_DEGRADED

        if (sign == "+"):
            color = "#9ec600"
        else:
            color = "#c68b00"

        if (self.minimized == False):
            return ( self.py3.safe_format(self.format, dict(stock=self.stock_display_name, price=price, percent=percent),),color,)
        else:
            return ( self.py3.safe_format(self.format_minimized, dict(stock=self.stock_display_name, price=price, percent=percent),),color,)

    def on_click(self, event):
        """
        """
        button = event["button"]
        if button == self.button_minimize:
            if self.minimized == False:
                self.minimized = True
            else:
                self.minimized = False
        elif button == self.button_next:
            self.stock_info_index += 1
            if self.stock_info_index >= len(self.stock_info):
                self.stock_info_index = 0
        sleep(0.1)

    def stocks(self):
        (text, color) = self._get_text()
        response = {
            "cached_until": self.py3.time_in(self.cache_timeout),
            "color": color,
            "full_text": text,
        }
        return response

if __name__ == "__main__":
    """
    Run module in test mode.
    """

    from py3status.module_test import module_test

    module_test(Py3status)
